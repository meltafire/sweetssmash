﻿using UnityEngine;
using System.Collections;

public class Constants{
	public static readonly int Rows = 9;
	public static readonly int Columns = 9;

	public static readonly float AnimationDuration = .2f;
	public static readonly float MoveAnimationMinDuration = .05f;
	public static readonly float ExplosionDuration = .3f;

	public static readonly float WaitBeforePotentialmatchesCheck = 2f;
	public static readonly float OpacityAnimationFrameDelay = .05f;

	public static readonly int MinimumMatches =3;
	public static readonly int MinimumMatchesForBonus =4;

	public static readonly int Match3Score = 10;
	public static readonly int SubsequentMatchScore = 100;
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public Text ScoreText;

	public ShapesArray shapes;

	private int score;

	private Vector2 BottomLeft;
	public readonly Vector2 CandySize = new Vector2 (1f, 1f);

	private GameState state = GameState.Idle;
	private GameObject hitGo = null;
	private Vector2[] SpawnPositions;

	private GameObject[] CandyPrefabs;
	private GameObject[] ExplosionPrefabs;
	private GameObject[] BonusPrefabs;

	private IEnumerator CheckPotentialMatchesCoroutine;
	private IEnumerator AnimatePotentialMatchesCoroutine;

	IEnumerable<GameObject> potentialMatches;

	public SoundManager soundManager;

	void Awake()
	{
		CalculateMapDisplacement ();
		LoadPrefabs ();
	}

	void Start()
	{
		InitializeTypesOnPrefabShapesAndBonuses ();
		InitializeCandyAndSpawnPositions ();
		StartCheckForPotentialMatches ();
	}

	void Update()
	{
		if (state == GameState.Idle) {
			if (Input.GetMouseButton (0)) {
				var hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
				if (hit.collider != null) {
					hitGo = hit.collider.gameObject;
					state = GameState.SelectionStarted;

				}
			}
		}
		 else if (state == GameState.SelectionStarted) 
		{
			if (Input.GetMouseButton(0))
			{
				var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
				if (hit.collider != null && hitGo != hit.collider.gameObject)
				{
					StopCheckForPotentialMatches();

					if (!Utilities.AreVerticalOrHorizontalNeighbors(hitGo.GetComponent<Shape>(),
					                                                hit.collider.gameObject.GetComponent<Shape>()))
					{
						state =GameState.Idle;
					}
					else
					{
						state = GameState.Animating;
						FixSortingLayer(hitGo, hit.collider.gameObject);
						StartCoroutine (FindMatchesAndCollapse(hit));
					}
				}
			}
		}
	}

	private void CalculateMapDisplacement()
	{
		float displacementX, displacementY;
		if ((float)Constants.Columns % 2 == 0) {
			displacementX = 0.5f-Constants.Columns/2;
		} else {
			displacementX = -Constants.Columns/2;
		}
		if ((float)Constants.Rows % 2 == 0) {
			displacementY =  0.5f-Constants.Rows/2;
		} else {
			displacementY = -Constants.Rows/2;
		}
		BottomLeft = new Vector2 (displacementX, displacementY);
	}

	private void LoadPrefabs()
	{
		CandyPrefabs = new GameObject[]{
			Resources.Load ("candy/Blue") as GameObject,
			Resources.Load ("candy/Green") as GameObject,
			Resources.Load ("candy/Orange") as GameObject,
			Resources.Load ("candy/Purple") as GameObject,
			Resources.Load ("candy/Red") as GameObject};
		
		ExplosionPrefabs = new GameObject[]{
			Resources.Load ("animation/Explosion_Blue") as GameObject,
			Resources.Load ("animation/Explosion_Green") as GameObject,
			Resources.Load ("animation/Explosion_Pink") as GameObject,
			Resources.Load ("animation/Explosion_Red") as GameObject};
		
		BonusPrefabs = new GameObject[]{
			Resources.Load ("bonus/RemoveH_Blue") as GameObject,
			Resources.Load ("bonus/RemoveH_Green") as GameObject,
			Resources.Load ("bonus/RemoveH_Orange") as GameObject,
			Resources.Load ("bonus/RemoveH_Purple") as GameObject,
			Resources.Load ("bonus/RemoveH_Red") as GameObject};
	}


	private void InitializeTypesOnPrefabShapesAndBonuses()
	{
		foreach (var item in CandyPrefabs)
			item.GetComponent<Shape>().Type = item.name;

		foreach (var item in BonusPrefabs) 
			item.GetComponent<Shape> ().Type = CandyPrefabs.Where(x => x.GetComponent<Shape>().Type.Contains(item.name.Split('_')[1].Trim())).Single().name;

	}

	private void InitializeCandyAndSpawnPositions()
	{
		InitializeVariables ();

		if (shapes != null)
			DestroyAllCandy ();

		shapes = new ShapesArray ();
		SpawnPositions = new Vector2[Constants.Columns];

		for (int row =0; row < Constants.Rows; row++)
		{
			for (int column =0; column < Constants.Columns; column++)
			{
				GameObject newCandy = GetRandomCandy();

				//2 previos in columns not the same
				while (column >=2 && shapes[row, column-1].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>())
				       && shapes[row, column-2].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>()))
				{
					newCandy = GetRandomCandy();
				}

				//2 previos in rows not the same
				while (row >=2 && shapes[row-1, column].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>())
				       && shapes[row-2, column].GetComponent<Shape>().IsSameType(newCandy.GetComponent<Shape>()))
				{
					newCandy = GetRandomCandy();
				}

				InstantiateAndPlaceNewCandy(row, column, newCandy);
			}
		}
		SetupSpawnPositions ();
	}

	private void InitializeVariables()
	{
		score = 0;
		ShowScore ();
	}

	private void ShowScore()
	{
		ScoreText.text = "Score: " + score.ToString ();
	}

	private void IncreaseScore(int amount)
	{
		score += amount;
		ShowScore();
	}

	private GameObject GetRandomCandy()
	{
		return CandyPrefabs[Random.Range(0, CandyPrefabs.Length)];
	}

	private void InstantiateAndPlaceNewCandy(int row, int column, GameObject newCandy)
	{
		GameObject go = Instantiate(
			newCandy, BottomLeft + new Vector2(column * CandySize.x, row * CandySize.y), Quaternion.identity) as GameObject;

		go.GetComponent<Shape> ().Assign (newCandy.GetComponent<Shape> ().Type, row, column);
		shapes [row, column] = go;
	}

	private void SetupSpawnPositions()
	{
		for (int column =0; column < Constants.Columns; column++)
			SpawnPositions [column] = BottomLeft + new Vector2 (column * CandySize.x, Constants.Rows * CandySize.y);
	}

	private void DestroyAllCandy()
	{
		for (int row =0; row < Constants.Rows; row++)
			for (int column = 0; column < Constants.Columns; column++)
				Destroy (shapes[row, column]);
	}

	private void FixSortingLayer(GameObject hitGo1, GameObject hitGo2)
	{
		SpriteRenderer sp1 = hitGo1.GetComponent<SpriteRenderer> ();
		SpriteRenderer sp2 = hitGo2.GetComponent<SpriteRenderer> ();
		if (sp1.sortingOrder <= sp2.sortingOrder)
		{
			sp1.sortingOrder = 1;
			sp2.sortingOrder = 0;
		}
	}

	private IEnumerator CheckPotentialMatches()
	{
		yield return new WaitForSeconds (Constants.WaitBeforePotentialmatchesCheck);
		potentialMatches = Utilities.GetPotentialMatches (shapes);
		if (potentialMatches != null)
			while (true)
			{
				AnimatePotentialMatchesCoroutine =Utilities.AnimatePotencialMatches(potentialMatches);
				StartCoroutine(AnimatePotentialMatchesCoroutine);
				yield return new WaitForSeconds(Constants.WaitBeforePotentialmatchesCheck);
			}
	}

	private void ResetOpacityOnPotentialMatches()
	{
		if (potentialMatches != null)
			foreach (var item in potentialMatches)
			{
			if (item==null)
				break;
			Color c = item.GetComponent<SpriteRenderer>().color;
			c.a = 1f;
			item.GetComponent<SpriteRenderer>().color =c;
			}
	}

	private void StartCheckForPotentialMatches ()
	{
		StopCheckForPotentialMatches ();

		CheckPotentialMatchesCoroutine = CheckPotentialMatches ();
		StartCoroutine (CheckPotentialMatchesCoroutine);

	}

	private void StopCheckForPotentialMatches()
	{
		if (AnimatePotentialMatchesCoroutine != null)
			StopCoroutine (AnimatePotentialMatchesCoroutine);
		if (CheckPotentialMatchesCoroutine !=null)
			StopCoroutine (CheckPotentialMatchesCoroutine);
		ResetOpacityOnPotentialMatches ();
	}

	private GameObject GetRandomExplosion()
	{
		return ExplosionPrefabs [Random.Range (0, ExplosionPrefabs.Length)];
	}

	private GameObject GetBonusFromType(string type)
	{
		foreach (var item in BonusPrefabs) 
		{
			if (item.GetComponent<Shape>().Type.Contains (type))
				return item;
		}
		throw new System.Exception ("Wrong type");
	}

	private void RemoveFromScene(GameObject item)
	{
		GameObject explosion = GetRandomExplosion ();
		var newExplosion = Instantiate (explosion, item.transform.position, Quaternion.identity) as GameObject;
		Destroy (newExplosion, Constants.ExplosionDuration);
		Destroy (item);
	}

	private void MoveAndAnimate(IEnumerable<GameObject> movedGameObjects, int distance)
	{
		foreach (var item in movedGameObjects) {
			item.transform.positionTo(
				Constants.MoveAnimationMinDuration * distance,
				BottomLeft + new Vector2(item.GetComponent<Shape>().Column*CandySize.x,
			                          item.GetComponent<Shape>().Row*CandySize.y));                      
		}
	}

	private AlteredCandyInfo CreateNewCandyInSpecificColumns(IEnumerable<int> columnsWithMissingCandy)
	{
		AlteredCandyInfo newCandyInfo = new AlteredCandyInfo();

		//how many nulls in column
		foreach (int column in columnsWithMissingCandy)
		{
			var emptyItems = shapes.GetEmptyItemsOnColumn(column);
			foreach (var item in emptyItems)
			{
				var go =GetRandomCandy();
				GameObject newCandy = Instantiate (go, SpawnPositions[column], Quaternion.identity) as GameObject;

				newCandy.GetComponent<Shape>().Assign(go.GetComponent<Shape>().Type, item.Row, item.Column);

				if (Constants.Rows - item.Row > newCandyInfo.MaxDistance)
					newCandyInfo.MaxDistance = Constants.Rows - item.Row;

				shapes[item.Row, item.Column] = newCandy;
				newCandyInfo.AddCandy(newCandy);
			}
		}
		return newCandyInfo;
	}

	private void CreateBonus (Shape hitGoCache)
	{
		GameObject bonus = Instantiate (GetBonusFromType (hitGoCache.Type), BottomLeft
		                                + new Vector2(hitGoCache.Column *CandySize.x, hitGoCache.Row*CandySize.y), Quaternion.identity) as GameObject;
		shapes [hitGoCache.Row, hitGoCache.Column] = bonus;
		var bonusShape = bonus.GetComponent<Shape> ();
		//same type as candy
		bonusShape.Assign (hitGoCache.Type, hitGoCache.Row, hitGoCache.Column);
		bonusShape.Bonus |= BonusType.DestroyWholeRowColumn;
	}

	private IEnumerator FindMatchesAndCollapse (RaycastHit2D hit2)
	{
		var hitGo2 = hit2.collider.gameObject;
		shapes.Swap (hitGo, hitGo2);

		hitGo.transform.positionTo (Constants.AnimationDuration, hitGo2.transform.position);
		hitGo2.transform.positionTo (Constants.AnimationDuration, hitGo.transform.position);
		yield return new WaitForSeconds (Constants.AnimationDuration);

		var hitGoMatchesInfo = shapes.GetMatches (hitGo);
		var hitGo2MatchesInfo = shapes.GetMatches (hitGo2);


		var totalMatches = hitGoMatchesInfo.MatchedCandy.Union (hitGo2MatchesInfo.MatchedCandy).Distinct ();

		if (totalMatches.Count () < Constants.MinimumMatches)
		{
			hitGo.transform.positionTo(Constants.AnimationDuration, hitGo2.transform.position);
			hitGo2.transform.positionTo(Constants.AnimationDuration, hitGo.transform.position);
			yield return new WaitForSeconds (Constants.AnimationDuration);
			shapes.UndoSwap();
		}

		bool addBonus = totalMatches.Count () >= Constants.MinimumMatchesForBonus &&
			!BonusTypeUtilities.ContainsDestroyWholeRowColumn (hitGoMatchesInfo.BonusesContained) &&
			!BonusTypeUtilities.ContainsDestroyWholeRowColumn (hitGo2MatchesInfo.BonusesContained);

		Shape hitGoCache = null;
		if (addBonus)
		{
			hitGoCache = new Shape();
			var sameTypeGo = hitGoMatchesInfo.MatchedCandy.Count() > 0 ? hitGo : hitGo2;
			var shape = sameTypeGo.GetComponent<Shape>();
			hitGoCache.Assign(shape.Type, shape.Row, shape.Column);
		}

		int timesRun = 1;
		while (totalMatches.Count () >= Constants.MinimumMatches)
		{
			IncreaseScore((totalMatches.Count()-2)*Constants.Match3Score);

			if (timesRun >=2)
				IncreaseScore(Constants.SubsequentMatchScore);

			soundManager.PlayAudio();

			foreach (var item in totalMatches)
			{
				shapes.Remove (item);
				RemoveFromScene (item);
			}

			if (addBonus)
				CreateBonus(hitGoCache);
			addBonus = false;

			var columns =totalMatches.Select(go =>go.GetComponent<Shape>().Column).Distinct ();

			var collapsedCandyInfo = shapes.Collapse(columns);
			var newCandyInfo = CreateNewCandyInSpecificColumns (columns);

			int maxDistance = Mathf.Max (collapsedCandyInfo.MaxDistance, newCandyInfo.MaxDistance);

			MoveAndAnimate(newCandyInfo.AlteredCandy, maxDistance);
			MoveAndAnimate(collapsedCandyInfo.AlteredCandy, maxDistance);

			yield return new WaitForSeconds(Constants.MoveAnimationMinDuration * maxDistance);

			//any new matches within new/collapsed ones
			totalMatches =shapes.GetMatches(collapsedCandyInfo.AlteredCandy).
				Union(shapes.GetMatches(newCandyInfo.AlteredCandy)).Distinct();

			timesRun++;
		}

		state = GameState.Idle;
		StartCheckForPotentialMatches ();
	}
}

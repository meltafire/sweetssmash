﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioClip audioClip;
	private AudioSource audioSource;

	// Use this for initialization
	void Awake () {
		audioSource = AddAudio (audioClip);
	}

	private AudioSource AddAudio(AudioClip ac)
	{
		AudioSource temp = gameObject.AddComponent<AudioSource>();
		temp.playOnAwake = false;
		temp.clip = audioClip;
		return temp;
	}

	public void PlayAudio()
	{
		audioSource.Play ();
	}
}

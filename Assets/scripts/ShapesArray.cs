﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ShapesArray{
	private GameObject backupGO1, backupGO2;
	private GameObject[,] shapes = new GameObject[Constants.Rows, Constants.Columns];

	public GameObject this[int row, int column]
	{
		get
		{
			try
			{
				return shapes[row, column];
			}
			catch (System.Exception ex)
			{
				throw;
			}
		}
		set
		{
			shapes[row,column] =value;
		}
	}

	public void Swap(GameObject go1, GameObject go2)
	{
		backupGO1 = go1;
		backupGO2 = go2;

		var go1Shape = go1.GetComponent<Shape> ();
		var go2Shape = go2.GetComponent<Shape> ();

		int go1Row = go1Shape.Row;
		int go1Column = go1Shape.Column;
		int go2Row = go2Shape.Row;
		int go2Column = go2Shape.Column;

		var temp = shapes [go1Row, go1Column];
		shapes [go1Row, go1Column] = shapes [go2Row, go2Column];
		shapes [go2Row, go2Column] = temp;

		Shape.SwapColumnRow (go1Shape, go2Shape);
	}

	public void UndoSwap()
	{
		if (backupGO1 == null || backupGO2 == null)
			throw new UnityException ("Backup is null");

		Swap (backupGO1, backupGO2);
	}

	private IEnumerable<GameObject> GetMatchesHorizontally (GameObject go)
	{
		List<GameObject> matches = new List<GameObject> ();
		matches.Add (go);
		var shape = go.GetComponent<Shape> ();
		//left
		if (shape.Column !=0)
			for(int column =shape.Column-1; column >= 0; column --)
			{
			if(shapes[shape.Row, column].GetComponent<Shape>().IsSameType(shape))
				matches.Add(shapes[shape.Row, column]);
			else
				break;
			}
		//right
		if (shape.Column !=Constants.Columns-1)
			for(int column =shape.Column+1; column < Constants.Columns; column ++)
		{
			if(shapes[shape.Row, column].GetComponent<Shape>().IsSameType(shape))
				matches.Add(shapes[shape.Row, column]);
			else
				break;
		}
		// wanted more than 3
		if (matches.Count < Constants.MinimumMatches)
			matches.Clear ();

		return matches.Distinct ();
	}

	private IEnumerable<GameObject> GetMatchesVertically(GameObject go)
	{
		List<GameObject> matches = new List<GameObject> ();
		matches.Add (go);
		var shape = go.GetComponent<Shape> ();
		//bottom
		if (shape.Row != 0)
			for (int row = shape.Row - 1; row >= 0; row --)
			{
				if (shapes[row, shape.Column] != null &&
				    shapes[row, shape.Column].GetComponent<Shape>().IsSameType(shape))
					matches.Add (shapes[row, shape.Column]);
				else
					break;
			}
		//top
		if (shape.Row != Constants.Rows-1)
			for (int row = shape.Row + 1; row < Constants.Rows; row ++)
			{
				if (shapes[row, shape.Column] != null &&
			    	shapes[row, shape.Column].GetComponent<Shape>().IsSameType(shape))
					matches.Add (shapes[row, shape.Column]);
				else
					break;
			}
		// wanted more than 3
		if (matches.Count < Constants.MinimumMatches)
			matches.Clear ();

		return matches.Distinct ();
	}

	private IEnumerable<GameObject> GetEntireRow(GameObject go)
	{
		List<GameObject> matches = new List<GameObject> ();
		int row = go.GetComponent<Shape> ().Row;
		for (int column = 0; column<Constants.Columns; column++) {
			//Debug.Log("Rrow " + row + " col " + column);
			matches.Add (shapes [row, column]);
		}
		return matches;
	}

	private IEnumerable<GameObject> GetEntireColumn(GameObject go)
	{
		List<GameObject> matches = new List<GameObject> ();
		int column = go.GetComponent<Shape> ().Column;
		for (int row = 0; row<Constants.Rows; row++) {
			//Debug.Log("Crow " + row + " col " + column);
			matches.Add (shapes [row, column]);
		}
		return matches;
	}

	private bool ContainsDestroyRowColumnBonus(IEnumerable<GameObject> matches)
	{
		if (matches.Count () >= Constants.MinimumMatches)
		{
			foreach(var go in matches)
				if(BonusTypeUtilities.ContainsDestroyWholeRowColumn(go.GetComponent<Shape>().Bonus))
					return true;
		}
		return false;
	}

	public MatchesInfo GetMatches(GameObject go)
	{
		MatchesInfo matchesInfo = new MatchesInfo ();

		var horizontalMatches = GetMatchesHorizontally (go);
		if (ContainsDestroyRowColumnBonus (horizontalMatches))
		{
			horizontalMatches =GetEntireRow(go);
			if (!BonusTypeUtilities.ContainsDestroyWholeRowColumn(matchesInfo.BonusesContained))
				matchesInfo.BonusesContained |=BonusType.DestroyWholeRowColumn;
		}
		matchesInfo.AddObjectRange (horizontalMatches);

		var verticalMatches = GetMatchesVertically (go);
		if (ContainsDestroyRowColumnBonus (verticalMatches))
		{
			verticalMatches =GetEntireColumn(go);
			if (!BonusTypeUtilities.ContainsDestroyWholeRowColumn(matchesInfo.BonusesContained))
				matchesInfo.BonusesContained |=BonusType.DestroyWholeRowColumn;
		}
		matchesInfo.AddObjectRange (verticalMatches);

		return matchesInfo;
	}

	public IEnumerable<GameObject> GetMatches(IEnumerable<GameObject> gos)
	{
		List<GameObject> matches = new List<GameObject>();
		foreach (var go in gos)
			matches.AddRange (GetMatches(go).MatchedCandy);
		return matches.Distinct ();
	}

	public void Remove(GameObject go)
	{
		shapes [go.GetComponent<Shape> ().Row, go.GetComponent<Shape> ().Column] = null;
	}

	public AlteredCandyInfo Collapse(IEnumerable<int> columns)
	{
		AlteredCandyInfo collapseInfo = new AlteredCandyInfo ();
		foreach (var column in columns)
		{
			for (int row =0; row < Constants.Rows -1;row++)
			{
				if (shapes[row, column] == null)
				{
					for (int row2= row + 1; row2<Constants.Rows;row2++)
					{
						if (shapes[row2,column] != null)
						{
							shapes[row, column] = shapes[row2, column];
							shapes[row2, column] = null;

							if (row2-row > collapseInfo.MaxDistance)
								collapseInfo.MaxDistance = row2 - row;

							shapes[row, column].GetComponent<Shape>().Row =row;
							shapes[row, column].GetComponent<Shape>().Column = column;

							collapseInfo.AddCandy(shapes[row, column]);
							break;
						}
					}
				}
			}
		}
		return collapseInfo;
	}

	public IEnumerable<ShapeInfo> GetEmptyItemsOnColumn(int column)
	{
		List<ShapeInfo> emptyItems = new List<ShapeInfo> ();
		for (int row = 0; row<Constants.Rows; row++)
			if (shapes [row, column] == null)
				emptyItems.Add (
					new ShapeInfo() {Row = row, Column = column} );
		return emptyItems;
	}
}

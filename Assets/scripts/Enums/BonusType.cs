﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum BonusType
{
	None,
	DestroyWholeRowColumn
}
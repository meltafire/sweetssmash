﻿using UnityEngine;
using System.Collections;

public enum GameState
{
	Idle,
	SelectionStarted,
	Animating
}